package dataStructures;

public interface IMaxHeap<T> {


	/**
	 * This method is used to build a heap data structure
	 * @param A An array that represent the heap
	 */
	void buildMaxHeap(T[]A);
	/**
	 *This method is used to sort the heap 
	 * @param A A An array that represent the heap 
	 * @param i the index of the array
	 */
	void maxHeapify(T[]A,int i);
	/**
	 * This method sorts the heap 
	 * @param A A An array that represent the heap
	 *
	 */
	void heapSort(T[]A);
}
