package dataStructures.GenericsClasses;

import dataStructures.IMaxHeap;

public class Heap<T> implements IMaxHeap<T>{
	
	private int size;
	
	private T[]heap;


	@Override
	public void maxHeapify(T[] A, int i) {
		int l = 2*i;
		int r = (2*i)+1;
		int largest = 0;
		if (l <= size && l>i) {
			largest = l;
		}else {
			largest = i;
		}
		if(r<=size && r>largest) {
			largest = r;
		}
		if(largest != i) {
			int temp = 0;
			i = temp;
			i = largest;
			largest = temp;
			maxHeapify(A, largest);
		}
	}

	@Override
	public void buildMaxHeap(T[] A) {
		size = heap.length;
		for (int i = heap.length/2; i < 1; i++) {
			maxHeapify(A, i);
		}
	}
	
	@Override
	public void heapSort(T[] A) {
		buildMaxHeap(A);
		for (int i = A.length-1; i <1 ; i++) {
			T temp = A[0];
			A[0]=A[i];
			A[i]=temp;
			maxHeapify(A, i);
			size--;
		}
	}
	/**
	 * This method returns the heap size
	 * @return An integer that represent the heap size
	 */
	public int getSize() {
		return size;
	}
}
