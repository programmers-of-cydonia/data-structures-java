package dataStructures;


import dataStructures.Exception.ListException;
import dataStructures.GenericsClasses.Node;

public interface IList<E> {
	/**
	 * This method adds an element into the list
	 * @param Element The element to be added 
	 * @throws LinkedListException when the element could not be added
	 * @throws ListException 
	 */
	void addElement(E Element) throws  ListException;
	/**
	 * This method checks if the list is empty
	 * @return True if is empty false if not
	 */
	boolean isEmpty();
	/**
	 * This method delete the given element
	 * @param Element The element to be deleted
	 */
	void delete(E Element);
	/**
	 * This method delete ALL from the list
	 */
	void deleteAll();
	/**
	 * This method is used to search an access into the element
	 * @param int n an Integer that indicates the position 
	 * @return The element with the ID
	 */
	E getElement(int n)throws NullPointerException;
	/**
	 * This method is used to know the list size
	 * @return An integer that represent the list size
	 */
	int size();


}
